<?php
require_once("mc/MastodonClient.php");

define("DEBUG_OUTPUT_ENABLE", TRUE);
define("LOG_FILENAME", "log.txt");

// 可能なら前回動作の時刻を取得する
if ( ($serialized_log = @file_get_contents(LOG_FILENAME)) !== FALSE ) {
	$log = unserialize($serialized_log);
}
else {
	$log = array(
		"success_unix_timestamp"=>0,
		"csv_update_timestamp"=>0
	);
}

$nowDateTime = new DateTime();
$nowDateTime->setTimezone(new DateTimeZone("Asia/Tokyo"));
if ( DEBUG_OUTPUT_ENABLE ) {
	fprintf(STDERR, "prev_time:     ". $log['success_unix_timestamp'] .PHP_EOL);
	fprintf(STDERR, "now timestamp: ". $nowDateTime->getTimestamp().PHP_EOL);
}
if ( ($log['success_unix_timestamp']+180) > $nowDateTime->getTimestamp() ) {
	if ( DEBUG_OUTPUT_ENABLE ) {
		fprintf(STDERR, "前回の動作から 3 分経過していません。動作を中止します。". PHP_EOL);
	}
	return 0;
}

// CSV をダウンロードしてメモリに展開する
$text = file_get_contents("https://www.kansai-td.co.jp/yamasou/juyo1_kansai.csv");
if ( $text === FALSE ) {
	if ( DEBUG_OUTPUT_ENABLE ) {
		fprintf(STDERR, "Failed loading CSV data.".PHP_EOL);
	}
	exit(1);
}
$text = mb_convert_encoding($text, "UTF-8", "SJIS-win");
$csv = explode("\r\n", $text);

// CSV の更新時刻をチェック
$csvrow = explode(" ", $csv[0]);
$csv_update_time = DateTime::createFromFormat("Y/n/j H:i", $csvrow[0]." ".$csvrow[1], new DateTimeZone("Asia/Tokyo"));
if ( DEBUG_OUTPUT_ENABLE) {
	fprintf(STDERR, "CSV Update Time: ". $csv_update_time->format("Y-m-d H:i:s").PHP_EOL);
}
if ( $log['csv_update_timestamp'] >= $csv_update_time->getTimestamp() ) {
	if ( DEBUG_OUTPUT_ENABLE ) {
		fprintf(STDERR, "CSV が更新されていません。動作を中止します。". PHP_EOL);
	}
	return 0;
}

// CSV フォーマットのチェック
$validate_result = validate_csv_format($csv);
if ( DEBUG_OUTPUT_ENABLE ) {
	fprintf(STDERR, "Validate CSV Format: ". (($validate_result) ? "成功" : "失敗") . PHP_EOL);
}
if ( $validate_result !== TRUE ) {
	fprintf(STDERR, "ほわっ CSV のフォーマットが変わったみたいですよっ".PHP_EOL);
	exit(1);
}

// 最新の電力需要量を取得
$demand_data_array = array_slice($csv, 58);
$power_demand = 0;
$power_demand_time = "";
foreach ( $demand_data_array as $demand_data_row ) {
	$demand_information = explode(",", $demand_data_row);
	if ( strlen($demand_information[2]) == 0 ) {
		break;
	}
	$power_demand_date = $demand_information[0];
	$power_demand_time = $demand_information[1];
	$power_demand = intval($demand_information[2]);
}
if ( DEBUG_OUTPUT_ENABLE ) {
	fprintf(STDERR, "Demand Information: ". $power_demand_time . " / ". $power_demand . PHP_EOL);
}

if ( $power_demand == 0 ) {
	// 最新の電力需要量が取得できなかった
	fprintf(STDERR, "ほわっ 電気がどれだけ使われているのか分かりませんでしたっ". PHP_EOL);
	exit(1);
}

// 最新の電力需要の時刻情報を作成する
$demand_datetime = DateTime::createFromFormat("Y/n/j H:i", $power_demand_date." ".$power_demand_time, new DateTimeZone("Asia/Tokyo"));
if ( DEBUG_OUTPUT_ENABLE ) {
	fprintf(STDERR, "Power Demand Time: ". $demand_datetime->format("Y-m-d H:i:s"). PHP_EOL);
}
if ( isset($log["demand_update_timestamp"]) && ($log["demand_update_timestamp"] >= $demand_datetime->getTimestamp()) ) {
	if ( DEBUG_OUTPUT_ENABLE ) {
		fprintf(STDERR, "5 分間隔値が更新されていません。処理を終了します。".PHP_EOL);
	}
	return 0;
}

// 取得した最新の電力需要量に関連づく電力供給量を取得する
if ( DEBUG_OUTPUT_ENABLE ) {
	echo $csv[intval($demand_datetime->format("G")) + 17];
}
$supply_information = explode(",", $csv[intval($demand_datetime->format("G")) + 17]);
$peak_supply = $supply_information[5];

$sup_dem_ratio = round(($power_demand / $peak_supply) * 100, 2);

$add_warning_icon = ( $sup_dem_ratio < 95.0 ) ? '' : '⚠ ';
$visibility = ( $sup_dem_ratio < 98.0 ) ? MastodonClient::VISIBILITY_UNLISTED : MastodonClient::VISIBILITY_PUBLIC;

// トゥート文を作成する
$toot = $add_warning_icon ."関西電力管内における ". $demand_datetime->format("H:i") ." 時点の電力需給状況です。".PHP_EOL.PHP_EOL.$demand_datetime->format("G")." 時台供給力: ".$peak_supply." 万kW".PHP_EOL."電力需要量: ".$power_demand." 万kW".PHP_EOL."電力使用率: ". $sup_dem_ratio ."%";

// とぅーとする
$mc = new MastodonClient();
$mc->init();
$mc->post_statuses($visibility, $toot);

// CSV 更新時刻を保存する
$log_output = array(
	"success_unix_timestamp"=>$nowDateTime->getTimestamp(),
	"csv_update_timestamp"=>$csv_update_time->getTimestamp(),
	"demand_update_timestamp"=>$demand_datetime->getTimestamp()
);
file_put_contents(LOG_FILENAME, serialize($log_output));

function validate_csv_format( $csv )
{
	require("csv_validation_data.php");
	try {
		foreach ( $csv_validation_data_array as $validation_data_key => $validation_text ) {
			if ( mb_strlen($validation_text) != mb_strlen($csv[$validation_data_key]) ) {
				throw new Exception("Validation Error at CSV row ". $validation_data_key ." (String length not expecting)");
			}
			if ( mb_strpos($csv[$validation_data_key], $validation_text) !== 0 ) {
				throw new Exception("Validation Error at CSV row ". $validation_data_key ." (String is not match)");
			}
		}
	}
	catch ( Exception $e ) {
		if ( DEBUG_OUTPUT_ENABLE ) {
			echo $e->getMessage().PHP_EOL;
		}
		return FALSE;
	}
	return TRUE;
}